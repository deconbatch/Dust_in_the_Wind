/**
 * Dust in the Wind.
 * draws an interesting shape with the Vector field method using one of the generated formulae.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * created 2019.11.16
 */

void setup() {
  size(980, 980);
  colorMode(HSB, 360, 100, 100, 100);
  rectMode(CENTER);
  smooth();
  noLoop();
}

void draw() {
  int   frmMax    = 3;  // draw 3 frames
  int   rotateMax = 2;  // draw 2 times with 180 degree rotation
  int   imgMax    = 2;  // draw 2 pattern per one frame
  float initHue   = random(360.0);

  translate(width * 0.5, height * 0.5);
  for (int frmCnt = 0; frmCnt < frmMax; frmCnt++) {
    noiseSeed(frmCnt * 10);

    blendMode(BLEND);
    drawCanvas(initHue + 240.0);

    blendMode(DIFFERENCE);
    noStroke();
    noFill();
    pushMatrix();
    rotate(random(PI));
    for (int rotateCnt = 0; rotateCnt < rotateMax; rotateCnt++) {
      initHue += 30.0;
      rotate(TWO_PI / rotateMax);
      for (int imgCnt = 1; imgCnt <= imgMax; imgCnt++) {
        float imgRatio = map(imgCnt, 1, imgMax, 0.0, 1.0);
        int   plotMax  = floor(map(imgRatio, 0.0, 1.0, 600.0, 900.0));
        float plotDiv  = map(imgRatio, 0.0, 1.0, 0.001, 0.002);
        float plotMult = map(imgRatio, 0.0, 1.0, 5.0, 15.0);
        float initDiv  = map(imgRatio, 0.0, 1.0, 0.005, 0.015);
        float baseHue  = initHue + map(imgRatio, 0.0, 1.0, 0.0, 30.0);
        float baseBri  = 1.0;
        float baseSiz  = map(imgRatio, 0.0, 1.0, 0.7, 0.4);

        drawVector(plotMax, plotDiv, plotMult, initDiv, baseHue, baseBri, baseSiz);
      }
    }
    popMatrix();

    casing();
    saveFrame("frames/" + String.format("%04d", frmCnt + 1) + ".png");
  }      
  exit();
}

/**
 * drawVector : draw vector field
 * @param  _plotMax  : dot plot count per initial coordinates.
 * @param  _plotDiv  : dot plot increments.
 * @param  _plotMult : curvature, bigger value means large curvature.
 * @param  _initDiv  : initial coordinates increments.
 * @param  _baseHue  : dot hue.
 * @param  _baseBri  : dot brightness.
 * @param  _baseSiz  : dot size.
 */
private void drawVector(int _plotMax, float _plotDiv, float _plotMult, float _initDiv, float _baseHue, float _baseBri, float _baseSiz) {

  // draw vector field
  for (float xInit = -0.55; xInit <= 0.55; xInit += _initDiv) {
    for (float yInit = -0.55; yInit <= 0.55; yInit += _initDiv) {
      float xPoint = xInit;
      float yPoint = yInit;
      for (int plotCnt = 0; plotCnt < _plotMax; plotCnt++) {
        float plotRatio = map(plotCnt, 0, _plotMax, 0.0, 1.0);
        float eHue      = _baseHue + plotRatio * 30.0 + floor(((xInit * yInit) * 10000.0) % 4.0) * 10.0;
        float eSat      = map(sin(PI * plotRatio), 0.0, 1.0, 0.0, 60.0);
        float eBri      = _baseBri * (0.5 + sin(PI * plotRatio));
        float eSiz      = _baseSiz * (0.5 + sin(PI * plotRatio));

        float xPrev = xPoint;
        float yPrev = yPoint;

        // I found this formula by chance. I don't know all elements are necessary.
        float clc = cos(yPrev) * (sin(xPrev) + exp(xPrev) - atan(xPrev)) * log(yPrev) - (exp(yPrev) - cos(yPrev)) * exp(xPrev) - (sin(xPrev) % pow(yPrev, 2));

        // Add variation of shape with the Perlin noise.
        float noiseDiv = map(noise(xPrev * 1.0, yPrev * 1.0), 0.0, 1.0, 0.5, 1.5);
        xPoint += _plotDiv * cos((atan2(xPrev, clc) + TWO_PI * yPrev) * _plotMult * noiseDiv);
        yPoint += _plotDiv * sin((atan2(yPrev, clc) + TWO_PI * xPrev) * _plotMult * noiseDiv);

        fill(eHue % 360.0, eSat, eBri, 100.0);
        ellipse(xPoint * width * 0.45, yPoint * height * 0.45, eSiz, eSiz);
      }
    }
  }
}  

/**
 * drawCanvas : draw sand wall
 * @param  _baseHue : sand color.
 */
void drawCanvas(float _baseHue) {
  background(0.0, 0.0, 90.0, 100.0);
  for (int x = 0; x < width * 0.5; x += 2) {
    for (int y = 0; y < height * 0.5; y += 2) {
	
      float pSize = random(0.5, 1.0);
      float pDiv  = random(-2.0, 2.0);
      float pSat = 0.0;
      if ((x + y) % 3 == 0) {
        pSat = 80.0;
      }
      strokeWeight(pSize);
      stroke(_baseHue % 360.0, pSat, 30.0, 70.0);
      point(x + pDiv, y + pDiv);
      point(-x + pDiv, y + pDiv);
      point(x + pDiv, -y + pDiv);
      point(-x + pDiv, -y + pDiv);
    }
	}
}

/**
 * casing : draw fancy casing
 */
private void casing() {
  blendMode(BLEND);
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(54.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();
}

